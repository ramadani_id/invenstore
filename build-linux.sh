#!/bin/bash
echo "Building the API for linux 32 bit"
GOOS=linux GOARCH=386 go build -o bin/linux/32/invenstore
echo "Completed"
echo "Building the API for linux 64 bit"
GOOS=linux GOARCH=amd64 go build -o bin/linux/64/invenstore
echo "Completed"