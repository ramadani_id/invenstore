package main

import (
	"context"
	"database/sql"
	"flag"
	"invenstore/container"
	"invenstore/http"
	"invenstore/http/route"
	"invenstore/http/validator"
	"log"
	"os"
	"os/signal"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	dbName := flag.String("dbname", "invenstore.db", "SQLite Database Name")
	address := flag.String("address", ":4000", "API Host Address")

	flag.Parse()

	container := container.ServiceContainer()
	dbDsb, err := container.DbDsn(*dbName)
	if err != nil {
		log.Fatal(err)
	}

	db, err := sql.Open("sqlite3", dbDsb)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	migration := container.Migration(db)

	// Run migration
	if err := migration.Migrate(); err != nil {
		log.Fatal(err)
	}

	// Product
	productRepo := container.ProductRepository(db)
	productService := container.ProductService(productRepo)
	productImporter := container.ProductImporter(productRepo)

	// Purchase
	purchaseRepo := container.PurchaseRepository(db)
	purchaseService := container.PurchaseService(productService, purchaseRepo)
	purchaseImporter := container.PurchaseImporter(productService, purchaseRepo)

	// Sale
	saleRepo := container.SaleRepository(db)
	saleService := container.SaleService(saleRepo, purchaseRepo, productService)
	saleImporter := container.SaleImporter(saleRepo, purchaseRepo, productService)

	// Report
	reportRepo := container.ReportRepository(db)
	reportService := container.ReportService(reportRepo)

	// Controller
	productController := container.ProductController(
		validator.NewCreateProductValidator(productRepo),
		validator.NewUpdateProductValidator(productRepo),
		productService,
		productImporter,
	)

	purchaseController := container.PurchaseController(
		validator.NewPurchaseValidator(),
		validator.NewPurchasesReceivedHistoryValidator(),
		purchaseService,
		purchaseImporter,
	)
	saleController := container.SaleController(
		validator.NewSaleValidator(),
		saleService,
		saleImporter,
	)
	reportController := container.ReportController(reportService)

	e := echo.New()
	e.HideBanner = true
	e.HTTPErrorHandler = http.HTTPErrorHandler(e)
	e.Use(middleware.Logger())

	route.Route(e, &route.Controller{
		Product:  productController,
		Purchase: purchaseController,
		Sale:     saleController,
		Report:   reportController,
	})

	// Start server
	go func() {
		if err := e.Start(*address); err != nil {
			e.Logger.Info("Shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}
