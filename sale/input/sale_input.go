package input

type SaleInput struct {
	ProductID uint    `json:"product_id"`
	Quantity  int     `json:"quantity"`
	Price     float32 `json:"price"`
	OrderID   string  `json:"order_id"`
	IsSample  bool    `json:"is_sample"`
}
