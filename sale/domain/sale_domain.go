package domain

import (
	"invenstore/sale/enum"
	"time"
)

type SaleDomain struct {
	ID            uint        `json:"id"`
	ProductID     uint        `json:"-"`
	Quantity      int         `json:"quantity"`
	Price         float32     `json:"price"`
	PurchasePrice float32     `json:"purchase_price"`
	Total         float32     `json:"total"`
	OrderID       string      `json:"order_id"`
	Status        enum.Status `json:"status"`
	CreatedAt     time.Time   `json:"created_at"`
	UpdatedAt     time.Time   `json:"updated_at"`
}

func NewSaleDomain(productID uint, quantity int, price, purchasePrice float32, orderID string, isSample bool) (*SaleDomain, error) {
	var total float32
	var status enum.Status

	if !isSample {
		total = float32(quantity) * price
		status = enum.Ordered
	} else {
		price = 0
		purchasePrice = 0
		status = enum.Sample
	}

	return &SaleDomain{
		ProductID:     productID,
		Quantity:      quantity,
		Price:         price,
		PurchasePrice: purchasePrice,
		Total:         total,
		OrderID:       orderID,
		Status:        status,
		CreatedAt:     time.Now(),
		UpdatedAt:     time.Now(),
	}, nil
}

func (d *SaleDomain) SetBroken() error {
	d.Price = 0
	d.PurchasePrice = 0
	d.Total = 0
	d.Status = enum.Broken
	d.UpdatedAt = time.Now()

	return nil
}

func (d *SaleDomain) SetMissing() error {
	d.Price = 0
	d.PurchasePrice = 0
	d.Total = 0
	d.Status = enum.Missing
	d.UpdatedAt = time.Now()

	return nil
}

func (d *SaleDomain) SetTimestamps(createdAt, updatedAt int64) {
	d.CreatedAt = time.Unix(createdAt, 0)
	d.UpdatedAt = time.Unix(updatedAt, 0)
}
