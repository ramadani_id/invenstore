package enum

import "encoding/json"

type Status uint

const (
	Ordered Status = iota
	Broken
	Missing
	Sample
)

func (e Status) String() string {
	values := map[Status]string{
		Ordered: "ordered",
		Broken:  "broken",
		Missing: "missing",
		Sample:  "sample",
	}

	if val, ok := values[e]; ok {
		return val
	}

	return values[Ordered]
}

func (e *Status) MarshalJSON() ([]byte, error) {
	return json.Marshal(e.String())
}
