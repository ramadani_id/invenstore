package repository

import (
	"database/sql"
	"invenstore/errs"
	"invenstore/sale/domain"
	"time"
)

type SaleRepositorySQLite struct {
	db *sql.DB
}

const (
	CreateQuery = `
		INSERT INTO sales (product_id, quantity, price, purchase_price, total, order_id,
			status, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
	`
	UpdateQuery = `
		UPDATE sales SET product_id = ?, quantity = ?, price = ?, purchase_price = ?, total = ?,
			order_id = ?, status = ?, updated_at = ? WHERE id = ?
	`
	FindByIDQuery = `
		SELECT id, product_id, quantity, price, purchase_price, total, order_id,
			status, created_at, updated_at FROM sales WHERE id = ?
	`
	FindByProductIDAndCreatedAtQuery = `
		SELECT id, product_id, quantity, price, purchase_price, total, order_id,
			status, created_at, updated_at FROM sales WHERE product_id = ? AND created_at = ?
		LIMIT 1
	`
	FindOneQuery = `
		SELECT s.id, s.product_id, p.sku, p.name, s.quantity, s.price, s.purchase_price, s.total,
			s.order_id, s.status, s.created_at, s.updated_at
		FROM sales AS s
		JOIN products AS p ON s.product_id = p.id
		WHERE s.id = ?
	`
	FetchQuery = `
		SELECT s.id, s.product_id, p.sku, p.name, s.quantity, s.price, s.purchase_price, s.total,
			s.order_id, s.status, s.created_at, s.updated_at
		FROM sales AS s
		JOIN products AS p ON s.product_id = p.id
		ORDER BY s.created_at DESC
		LIMIT ?, ?
	`
	TotalQuery = "SELECT COUNT(id) as total FROM sales"
)

func NewSaleRepositorySQLite(db *sql.DB) SaleRepository {
	return &SaleRepositorySQLite{db}
}

func (r *SaleRepositorySQLite) Create(sale *domain.SaleDomain) (uint, error) {
	var id uint

	stmt, err := r.db.Prepare(CreateQuery)
	if err != nil {
		return id, err
	}
	defer stmt.Close()

	result, err := stmt.Exec(sale.ProductID, sale.Quantity, sale.Price, sale.PurchasePrice,
		sale.Total, sale.OrderID, sale.Status, sale.CreatedAt.Unix(), sale.UpdatedAt.Unix())
	if err != nil {
		return id, err
	}

	lastID, err := result.LastInsertId()
	if err != nil {
		return id, err
	}

	id = uint(lastID)

	return id, nil
}

func (r *SaleRepositorySQLite) Update(sale *domain.SaleDomain) error {
	stmt, err := r.db.Prepare(UpdateQuery)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(sale.ProductID, sale.Quantity, sale.Price, sale.PurchasePrice,
		sale.Total, sale.OrderID, sale.Status, sale.UpdatedAt.Unix(), sale.ID)
	if err != nil {
		return err
	}

	return nil
}

func (r *SaleRepositorySQLite) FindByID(id uint) (*domain.SaleDomain, error) {
	var createdAt int64
	var updatedAt int64

	result := &domain.SaleDomain{}

	row := r.db.QueryRow(FindByIDQuery, id)
	err := row.Scan(&result.ID, &result.ProductID, &result.Quantity, &result.Price, &result.PurchasePrice,
		&result.Total, &result.OrderID, &result.Status, &createdAt, &updatedAt)
	if err != nil && err == sql.ErrNoRows {
		return result, errs.EntityNotFound
	} else if err != nil {
		return result, err
	}

	result.SetTimestamps(createdAt, updatedAt)

	return result, nil
}

func (r *SaleRepositorySQLite) FindByProductIDAndCreatedAt(productID uint, createdAt time.Time) (*domain.SaleDomain, error) {
	var createdAtUnix int64
	var updatedAtUnix int64

	result := &domain.SaleDomain{}

	row := r.db.QueryRow(FindByProductIDAndCreatedAtQuery, productID, createdAt.Unix())
	err := row.Scan(&result.ID, &result.ProductID, &result.Quantity, &result.Price, &result.PurchasePrice,
		&result.Total, &result.OrderID, &result.Status, &createdAtUnix, &updatedAtUnix)
	if err != nil && err == sql.ErrNoRows {
		return result, errs.EntityNotFound
	} else if err != nil {
		return result, err
	}

	result.SetTimestamps(createdAtUnix, updatedAtUnix)

	return result, nil
}

func (r *SaleRepositorySQLite) FindOne(id uint) (*domain.ProductSaleDomain, error) {
	var createdAt int64
	var updatedAt int64

	result := &domain.ProductSaleDomain{}

	row := r.db.QueryRow(FindOneQuery, id)
	err := row.Scan(&result.ID, &result.Product.ID, &result.Product.SKU, &result.Product.Name, &result.Quantity,
		&result.Price, &result.PurchasePrice, &result.Total, &result.OrderID, &result.Status, &createdAt, &updatedAt)
	if err != nil && err == sql.ErrNoRows {
		return result, errs.EntityNotFound
	} else if err != nil {
		return result, err
	}

	result.SetTimestamps(createdAt, updatedAt)

	return result, nil
}

func (r *SaleRepositorySQLite) Fetch(offset, limit uint) ([]*domain.ProductSaleDomain, error) {
	results := make([]*domain.ProductSaleDomain, 0)

	rows, err := r.db.Query(FetchQuery, offset, limit)
	if err != nil {
		return results, err
	}
	defer rows.Close()

	for rows.Next() {
		var createdAt int64
		var updatedAt int64

		result := &domain.ProductSaleDomain{}

		err := rows.Scan(&result.ID, &result.Product.ID, &result.Product.SKU, &result.Product.Name,
			&result.Quantity, &result.Price, &result.PurchasePrice, &result.Total, &result.OrderID,
			&result.Status, &createdAt, &updatedAt)
		if err != nil {
			return results, err
		}

		result.SetTimestamps(createdAt, updatedAt)

		results = append(results, result)
	}

	return results, nil
}

func (r *SaleRepositorySQLite) Total() (uint, error) {
	var total uint

	if err := r.db.QueryRow(TotalQuery).Scan(&total); err != nil {
		return total, err
	}

	return total, nil
}
