package repository

import (
	"invenstore/sale/domain"
	"time"
)

type SaleRepository interface {
	Create(sale *domain.SaleDomain) (uint, error)
	Update(sale *domain.SaleDomain) error
	FindByID(id uint) (*domain.SaleDomain, error)
	FindByProductIDAndCreatedAt(productID uint, createdAt time.Time) (*domain.SaleDomain, error)
	FindOne(id uint) (*domain.ProductSaleDomain, error)
	Fetch(offset, limit uint) ([]*domain.ProductSaleDomain, error)
	Total() (uint, error)
}
