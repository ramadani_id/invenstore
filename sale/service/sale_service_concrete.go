package service

import (
	ps "invenstore/product/service"
	pr "invenstore/purchase/repository"
	"invenstore/sale/domain"
	"invenstore/sale/input"
	"invenstore/sale/repository"
)

type SaleServiceConcrete struct {
	saleRepo       repository.SaleRepository
	purchaseRepo   pr.PurchaseRepository
	productService ps.ProductService
}

func NewSaleServiceConcrete(
	saleRepo repository.SaleRepository,
	purchaseRepo pr.PurchaseRepository,
	productService ps.ProductService,
) SaleService {
	return &SaleServiceConcrete{saleRepo, purchaseRepo, productService}
}

func (s *SaleServiceConcrete) Create(input *input.SaleInput) (*domain.ProductSaleDomain, error) {
	result := &domain.ProductSaleDomain{}

	// Find latest purchase
	purchase, err := s.purchaseRepo.FindLatestByProductID(input.ProductID)
	if err != nil {
		return result, err
	}

	// Create sale
	sale, err := domain.NewSaleDomain(input.ProductID, input.Quantity, input.Price,
		purchase.Price, input.OrderID, input.IsSample)
	if err != nil {
		return result, err
	}

	id, err := s.saleRepo.Create(sale)
	if err != nil {
		return result, err
	}

	// Reduce product stock
	if err = s.productService.ReduceStock(sale.Quantity, sale.ProductID); err != nil {
		return result, err
	}

	return s.Find(id)
}

func (s *SaleServiceConcrete) Find(id uint) (*domain.ProductSaleDomain, error) {
	result, err := s.saleRepo.FindOne(id)
	if err != nil {
		return result, err
	}

	return result, nil
}

func (s *SaleServiceConcrete) SetBroken(id uint) (*domain.ProductSaleDomain, error) {
	result := &domain.ProductSaleDomain{}

	// Get sale
	sale, err := s.saleRepo.FindByID(id)
	if err != nil {
		return result, err
	}

	if err = sale.SetBroken(); err != nil {
		return result, err
	}

	// Update sale record
	if err = s.saleRepo.Update(sale); err != nil {
		return result, err
	}

	return s.Find(id)
}

func (s *SaleServiceConcrete) SetMissing(id uint) (*domain.ProductSaleDomain, error) {
	result := &domain.ProductSaleDomain{}

	// Get sale
	sale, err := s.saleRepo.FindByID(id)
	if err != nil {
		return result, err
	}

	if err = sale.SetMissing(); err != nil {
		return result, err
	}

	// Update sale record
	if err = s.saleRepo.Update(sale); err != nil {
		return result, err
	}

	return s.Find(id)
}

func (s *SaleServiceConcrete) Paginate(page, perPage uint) ([]*domain.ProductSaleDomain, uint, error) {
	var total uint
	results := make([]*domain.ProductSaleDomain, 0)
	offset := (page - 1) * perPage

	results, err := s.saleRepo.Fetch(offset, perPage)
	if err != nil {
		return results, total, err
	}

	total, err = s.saleRepo.Total()
	if err != nil {
		return results, total, err
	}

	return results, total, nil
}
