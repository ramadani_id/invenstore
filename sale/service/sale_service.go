package service

import (
	"invenstore/sale/domain"
	"invenstore/sale/input"
)

type SaleService interface {
	Create(input *input.SaleInput) (*domain.ProductSaleDomain, error)
	Find(id uint) (*domain.ProductSaleDomain, error)
	SetBroken(id uint) (*domain.ProductSaleDomain, error)
	SetMissing(id uint) (*domain.ProductSaleDomain, error)
	Paginate(page, perPage uint) ([]*domain.ProductSaleDomain, uint, error)
}
