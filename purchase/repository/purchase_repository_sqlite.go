package repository

import (
	"database/sql"
	"invenstore/errs"
	"invenstore/purchase/domain"
	"time"
)

type PurchaseRepositorySQLite struct {
	db *sql.DB
}

const (
	CreateQuery = `
		INSERT INTO purchases (product_id, quantity, received_quantity, price, total, receipt_no,
			status, histories, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
	`
	UpdateQuery = `
		UPDATE purchases SET product_id = ?, quantity = ?, received_quantity = ?, price = ?, total = ?,
			receipt_no = ?, status = ?, histories = ?, updated_at = ? WHERE id = ?
	`
	FindByIDQuery = `
		SELECT id, product_id, quantity, received_quantity, price, total, receipt_no, status,
			histories, created_at, updated_at FROM purchases WHERE id = ?
	`
	FindByProductIDAndCreatedAtQuery = `
		SELECT id, product_id, quantity, received_quantity, price, total, receipt_no, status,
			histories, created_at, updated_at FROM purchases WHERE product_id = ? AND created_at = ?
		LIMIT 1
	`
	FindLatestByProductIDQuery = `
		SELECT id, product_id, quantity, received_quantity, price, total, receipt_no, status,
			histories, created_at, updated_at FROM purchases WHERE product_id = ?
		ORDER BY created_at DESC LIMIT 1
	`
	FindOneQuery = `
		SELECT p.id, p.product_id, pd.sku, pd.name, p.quantity, p.received_quantity, p.price, p.total,
			p.receipt_no, p.status, p.histories, p.created_at, p.updated_at
		FROM purchases AS p
		JOIN products AS pd ON p.product_id = pd.id
		WHERE p.id = ?
	`
	FetchQuery = `
		SELECT p.id, p.product_id, pd.sku, pd.name, p.quantity, p.received_quantity, p.price, p.total,
			p.receipt_no, p.status, p.histories, p.created_at, p.updated_at
		FROM purchases AS p
		JOIN products AS pd ON p.product_id = pd.id
		ORDER BY p.created_at DESC
		LIMIT ?, ?
	`
	TotalQuery = "SELECT COUNT(id) as total FROM purchases"
)

func NewPurchaseRepositorySQLite(db *sql.DB) PurchaseRepository {
	return &PurchaseRepositorySQLite{db}
}

func (r *PurchaseRepositorySQLite) Create(purchase *domain.PurchaseDomain) (uint, error) {
	var id uint

	stmt, err := r.db.Prepare(CreateQuery)
	if err != nil {
		return id, err
	}
	defer stmt.Close()

	histories, err := purchase.GetHistories()
	if err != nil {
		return id, err
	}

	result, err := stmt.Exec(purchase.ProductID, purchase.Quantity, purchase.ReceivedQuantity,
		purchase.Price, purchase.Total, purchase.ReceiptNo, purchase.Status, histories,
		purchase.CreatedAt.Unix(), purchase.UpdatedAt.Unix())
	if err != nil {
		return id, err
	}

	lastID, err := result.LastInsertId()
	if err != nil {
		return id, err
	}

	id = uint(lastID)

	return id, nil
}

func (r *PurchaseRepositorySQLite) Update(purchase *domain.PurchaseDomain) error {
	stmt, err := r.db.Prepare(UpdateQuery)
	if err != nil {
		return err
	}
	defer stmt.Close()

	histories, err := purchase.GetHistories()
	if err != nil {
		return err
	}

	_, err = stmt.Exec(purchase.ProductID, purchase.Quantity, purchase.ReceivedQuantity,
		purchase.Price, purchase.Total, purchase.ReceiptNo, purchase.Status, histories,
		purchase.UpdatedAt.Unix(), purchase.ID)
	if err != nil {
		return err
	}

	return nil
}

func (r *PurchaseRepositorySQLite) FindByID(id uint) (*domain.PurchaseDomain, error) {
	var histories string
	var createdAt int64
	var updatedAt int64

	result := &domain.PurchaseDomain{}

	row := r.db.QueryRow(FindByIDQuery, id)
	err := row.Scan(&result.ID, &result.ProductID, &result.Quantity, &result.ReceivedQuantity, &result.Price,
		&result.Total, &result.ReceiptNo, &result.Status, &histories, &createdAt, &updatedAt)
	if err != nil && err == sql.ErrNoRows {
		return result, errs.EntityNotFound
	} else if err != nil {
		return result, err
	}

	if err = result.SetHistories(histories); err != nil {
		return result, err
	}

	result.SetTimestamps(createdAt, updatedAt)

	return result, nil
}

func (r *PurchaseRepositorySQLite) FindByProductIDAndCreatedAt(productID uint, createdAt time.Time) (*domain.PurchaseDomain, error) {
	var histories string
	var createdAtUnix int64
	var updatedAtUnix int64

	result := &domain.PurchaseDomain{}

	row := r.db.QueryRow(FindByProductIDAndCreatedAtQuery, productID, createdAt.Unix())
	err := row.Scan(&result.ID, &result.ProductID, &result.Quantity, &result.ReceivedQuantity, &result.Price,
		&result.Total, &result.ReceiptNo, &result.Status, &histories, &createdAtUnix, &updatedAtUnix)
	if err != nil && err == sql.ErrNoRows {
		return result, errs.EntityNotFound
	} else if err != nil {
		return result, err
	}

	if err = result.SetHistories(histories); err != nil {
		return result, err
	}

	result.SetTimestamps(createdAtUnix, updatedAtUnix)

	return result, nil
}

func (r *PurchaseRepositorySQLite) FindLatestByProductID(productID uint) (*domain.PurchaseDomain, error) {
	var histories string
	var createdAt int64
	var updatedAt int64

	result := &domain.PurchaseDomain{}

	row := r.db.QueryRow(FindLatestByProductIDQuery, productID)
	err := row.Scan(&result.ID, &result.ProductID, &result.Quantity, &result.ReceivedQuantity, &result.Price,
		&result.Total, &result.ReceiptNo, &result.Status, &histories, &createdAt, &updatedAt)
	if err != nil && err == sql.ErrNoRows {
		return result, errs.EntityNotFound
	} else if err != nil {
		return result, err
	}

	if err = result.SetHistories(histories); err != nil {
		return result, err
	}

	result.SetTimestamps(createdAt, updatedAt)

	return result, nil
}

func (r *PurchaseRepositorySQLite) FindOne(id uint) (*domain.ProductPurchaseDomain, error) {
	var histories string
	var createdAt int64
	var updatedAt int64

	result := &domain.ProductPurchaseDomain{}

	row := r.db.QueryRow(FindOneQuery, id)
	err := row.Scan(&result.ID, &result.Product.ID, &result.Product.SKU, &result.Product.Name, &result.Quantity,
		&result.ReceivedQuantity, &result.Price, &result.Total, &result.ReceiptNo, &result.Status, &histories,
		&createdAt, &updatedAt)
	if err != nil && err == sql.ErrNoRows {
		return result, errs.EntityNotFound
	} else if err != nil {
		return result, err
	}

	if err = result.SetHistories(histories); err != nil {
		return result, err
	}

	result.SetTimestamps(createdAt, updatedAt)

	return result, nil
}

func (r *PurchaseRepositorySQLite) Fetch(offset, limit uint) ([]*domain.ProductPurchaseDomain, error) {
	results := make([]*domain.ProductPurchaseDomain, 0)

	rows, err := r.db.Query(FetchQuery, offset, limit)
	if err != nil {
		return results, err
	}
	defer rows.Close()

	for rows.Next() {
		var histories string
		var createdAt int64
		var updatedAt int64

		result := &domain.ProductPurchaseDomain{}

		err := rows.Scan(&result.ID, &result.Product.ID, &result.Product.SKU, &result.Product.Name, &result.Quantity,
			&result.ReceivedQuantity, &result.Price, &result.Total, &result.ReceiptNo, &result.Status, &histories,
			&createdAt, &updatedAt)
		if err != nil {
			return results, err
		}

		if err = result.SetHistories(histories); err != nil {
			return results, err
		}

		result.SetTimestamps(createdAt, updatedAt)

		results = append(results, result)
	}

	return results, nil
}

func (r *PurchaseRepositorySQLite) Total() (uint, error) {
	var total uint

	if err := r.db.QueryRow(TotalQuery).Scan(&total); err != nil {
		return total, err
	}

	return total, nil
}
