package repository

import (
	"invenstore/purchase/domain"
	"time"
)

type PurchaseRepository interface {
	Create(purchase *domain.PurchaseDomain) (uint, error)
	Update(purchase *domain.PurchaseDomain) error
	FindByID(id uint) (*domain.PurchaseDomain, error)
	FindByProductIDAndCreatedAt(productID uint, createdAt time.Time) (*domain.PurchaseDomain, error)
	FindLatestByProductID(productID uint) (*domain.PurchaseDomain, error)
	FindOne(id uint) (*domain.ProductPurchaseDomain, error)
	Fetch(offset, limit uint) ([]*domain.ProductPurchaseDomain, error)
	Total() (uint, error)
}
