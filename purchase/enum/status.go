package enum

import "encoding/json"

type Status uint

const (
	Created Status = iota
	Uncompleted
	Completed
)

func (e Status) String() string {
	values := map[Status]string{
		Created:     "created",
		Uncompleted: "uncompleted",
		Completed:   "completed",
	}

	if val, ok := values[e]; ok {
		return val
	}

	return values[Created]
}

func (e *Status) MarshalJSON() ([]byte, error) {
	return json.Marshal(e.String())
}
