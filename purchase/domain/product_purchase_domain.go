package domain

type ProductPurchaseDomain struct {
	PurchaseDomain
	Product Product `json:"product"`
}

type Product struct {
	ID   uint   `json:"id"`
	SKU  string `json:"sku"`
	Name string `json:"name"`
}
