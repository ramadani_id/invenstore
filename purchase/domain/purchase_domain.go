package domain

import (
	"encoding/json"
	"invenstore/purchase/enum"
	"time"
)

type PurchaseDomain struct {
	ID               uint              `json:"id"`
	ProductID        uint              `json:"-"`
	Quantity         int               `json:"quantity"`
	ReceivedQuantity int               `json:"received_quantity"`
	Price            float32           `json:"price"`
	Total            float32           `json:"total"`
	ReceiptNo        string            `json:"receipt_no"`
	Status           enum.Status       `json:"status"`
	Histories        []ReceivedHistory `json:"histories"`
	CreatedAt        time.Time         `json:"created_at"`
	UpdatedAt        time.Time         `json:"updated_at"`
}

type ReceivedHistory struct {
	ReceivedAt time.Time `json:"received_at"`
	Quantity   int       `json:"quantity"`
}

func NewPurchaseDomain(productID uint, quantity int, price float32, receiptNo string) (*PurchaseDomain, error) {
	total := float32(quantity) * price
	return &PurchaseDomain{
		ProductID:        productID,
		Quantity:         quantity,
		ReceivedQuantity: 0,
		Price:            price,
		Total:            total,
		ReceiptNo:        receiptNo,
		Status:           enum.Created,
		Histories:        make([]ReceivedHistory, 0),
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}, nil
}

func (d *PurchaseDomain) Update(productID uint, quantity int, price float32, receiptNo string) error {
	total := float32(quantity) * price

	d.ProductID = productID
	d.Quantity = quantity
	d.Price = price
	d.Total = total
	d.ReceiptNo = receiptNo
	d.UpdatedAt = time.Now()

	return nil
}

func (d *PurchaseDomain) AddHistory(receivedAt time.Time, quantity int) error {
	// Append history
	d.Histories = append(d.Histories, ReceivedHistory{
		ReceivedAt: receivedAt,
		Quantity:   quantity,
	})

	totalReceived := 0
	for _, h := range d.Histories {
		totalReceived += h.Quantity
	}

	// Update status
	if totalReceived >= d.Quantity {
		d.Status = enum.Completed
	} else {
		d.Status = enum.Uncompleted
	}

	// Update received quantity
	d.ReceivedQuantity = totalReceived

	return nil
}

func (d *PurchaseDomain) GetHistories() (string, error) {
	bytes, err := json.Marshal(d.Histories)
	if err != nil {
		return "", err
	}

	return string(bytes), nil
}

func (d *PurchaseDomain) SetHistories(data string) error {
	histories := make([]ReceivedHistory, 0)
	if err := json.Unmarshal([]byte(data), &histories); err != nil {
		return err
	}

	d.Histories = histories

	return nil
}

func (d *PurchaseDomain) SetTimestamps(createdAt, updatedAt int64) {
	d.CreatedAt = time.Unix(createdAt, 0)
	d.UpdatedAt = time.Unix(updatedAt, 0)
}
