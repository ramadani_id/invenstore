package service

import (
	"invenstore/product/service"
	"invenstore/purchase/domain"
	"invenstore/purchase/input"
	"invenstore/purchase/repository"
	"time"
)

type PurchaseServiceConcrete struct {
	productService service.ProductService
	purchaseRepo   repository.PurchaseRepository
}

func NewPurchaseServiceConcrete(
	productService service.ProductService,
	purchaseRepo repository.PurchaseRepository,
) PurchaseService {
	return &PurchaseServiceConcrete{productService, purchaseRepo}
}

func (s *PurchaseServiceConcrete) Create(input *input.ProductPurchaseInput) (*domain.ProductPurchaseDomain, error) {
	result := &domain.ProductPurchaseDomain{}

	// Find or create product by sku and name
	product, err := s.productService.FindOrCreate(input.SKU, input.Name)
	if err != nil {
		return result, err
	}

	// Create pruchase domain
	purchase, err := domain.NewPurchaseDomain(product.ID, input.Quantity, input.Price, input.ReceiptNo)
	if err != nil {
		return result, err
	}

	// Create purchase
	id, err := s.purchaseRepo.Create(purchase)
	if err != nil {
		return result, err
	}

	return s.Find(id)
}

func (s *PurchaseServiceConcrete) Update(input *input.ProductPurchaseInput, id uint) (*domain.ProductPurchaseDomain, error) {
	result := &domain.ProductPurchaseDomain{}

	// Find or create product by sku and name
	product, err := s.productService.FindOrCreate(input.SKU, input.Name)
	if err != nil {
		return result, err
	}

	// Find pruchase
	purchase, err := s.purchaseRepo.FindByID(id)
	if err != nil {
		return result, err
	}

	// Update pruchase domain
	err = purchase.Update(product.ID, input.Quantity, input.Price, input.ReceiptNo)
	if err != nil {
		return result, err
	}

	// Update pruchase record
	if err = s.purchaseRepo.Update(purchase); err != nil {
		return result, err
	}

	return s.Find(id)
}

func (s *PurchaseServiceConcrete) Find(id uint) (*domain.ProductPurchaseDomain, error) {
	result, err := s.purchaseRepo.FindOne(id)
	if err != nil {
		return result, err
	}

	return result, nil
}

func (s *PurchaseServiceConcrete) AddHistory(receivedAt time.Time, quantity int, id uint) (*domain.ProductPurchaseDomain, error) {
	result := &domain.ProductPurchaseDomain{}

	// Find pruchase
	purchase, err := s.purchaseRepo.FindByID(id)
	if err != nil {
		return result, err
	}

	// Update purchase domain by add history
	if err = purchase.AddHistory(receivedAt, quantity); err != nil {
		return result, err
	}

	// Update pruchase record
	if err = s.purchaseRepo.Update(purchase); err != nil {
		return result, err
	}

	// Update product stock
	if err = s.productService.AddStock(quantity, purchase.ProductID); err != nil {
		return result, err
	}

	return s.Find(id)
}

func (s *PurchaseServiceConcrete) Paginate(page, perPage uint) ([]*domain.ProductPurchaseDomain, uint, error) {
	var total uint
	results := make([]*domain.ProductPurchaseDomain, 0)
	offset := (page - 1) * perPage

	results, err := s.purchaseRepo.Fetch(offset, perPage)
	if err != nil {
		return results, total, err
	}

	total, err = s.purchaseRepo.Total()
	if err != nil {
		return results, total, err
	}

	return results, total, nil
}
