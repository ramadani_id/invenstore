package service

import (
	"invenstore/purchase/domain"
	"invenstore/purchase/input"
	"time"
)

type PurchaseService interface {
	Create(input *input.ProductPurchaseInput) (*domain.ProductPurchaseDomain, error)
	Update(input *input.ProductPurchaseInput, id uint) (*domain.ProductPurchaseDomain, error)
	Find(id uint) (*domain.ProductPurchaseDomain, error)
	AddHistory(receivedAt time.Time, quantity int, id uint) (*domain.ProductPurchaseDomain, error)
	Paginate(page, perPage uint) ([]*domain.ProductPurchaseDomain, uint, error)
}
