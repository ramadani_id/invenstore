package input

type ProductPurchaseInput struct {
	SKU       string  `json:"sku"`
	Name      string  `json:"name"`
	Quantity  int     `json:"quantity"`
	Price     float32 `json:"price"`
	ReceiptNo string  `json:"receipt_no"`
}
