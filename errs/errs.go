package errs

import "errors"

var EntityNotFound = errors.New("EntityNotFound")
