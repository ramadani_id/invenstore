package importer

import "io"

type Importer interface {
	Import(reader io.Reader) error
}

func dateFormats() []string {
	return []string{
		"2006/01/02 15:04:05",
		"2006/01/02 15:04",
		"2006/1/02 15:04",
		"2006-01-02 15:04:05",
		"2006-01-02 15:04",
		"2006-1-02 15:04",
	}
}
