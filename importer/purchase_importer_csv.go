package importer

import (
	"encoding/csv"
	"invenstore/errs"
	"invenstore/product/service"
	"invenstore/purchase/domain"
	"invenstore/purchase/enum"
	"invenstore/purchase/repository"
	"io"
	"strconv"
	"strings"
	"time"
)

type PurchaseImporterCSV struct {
	purchaseRepo   repository.PurchaseRepository
	productService service.ProductService
}

func NewPurchaseImporterCSV(
	purchaseRepo repository.PurchaseRepository,
	productService service.ProductService,
) Importer {
	return &PurchaseImporterCSV{purchaseRepo, productService}
}

func (i *PurchaseImporterCSV) Import(reader io.Reader) error {
	r := csv.NewReader(reader)
	isHeader := true

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		if isHeader {
			isHeader = false
			continue
		}

		// Save purchase
		if err = i.save(record); err != nil {
			return err
		}
	}

	return nil
}

func (i *PurchaseImporterCSV) save(data []string) error {
	var createdAt time.Time

	for _, df := range dateFormats() {
		t, err := time.Parse(df, data[0])
		if err == nil {
			createdAt = t
			break
		}
	}

	if createdAt.IsZero() {
		createdAt = time.Now()
	}

	sku := data[1]
	name := data[2]
	quantity, err := strconv.Atoi(data[3])
	if err != nil {
		quantity = 0
	}
	receivedQuantity, err := strconv.Atoi(data[4])
	if err != nil {
		quantity = 0
	}
	price, err := strconv.ParseFloat(data[5], 32)
	if err != nil {
		price = 0
	}
	total, err := strconv.ParseFloat(data[6], 32)
	if err != nil {
		total = 0
	}
	receiptNo := data[7]
	if receiptNo == "(Hilang)" {
		receiptNo = ""
	}

	status := enum.Created
	if receivedQuantity >= quantity {
		status = enum.Completed
	} else if receivedQuantity < quantity {
		status = enum.Uncompleted
	}

	notes := data[8]
	histories, _ := i.parseHistories(notes)

	product, err := i.productService.FindOrCreate(sku, name)
	if err != nil {
		return err
	}

	purchase, err := i.purchaseRepo.FindByProductIDAndCreatedAt(product.ID, createdAt)
	if err == nil {
		// Update purchase or product stock

		// Get received quantity difference between old and new
		updatedStock := receivedQuantity - purchase.ReceivedQuantity

		purchase.Quantity = quantity
		purchase.ReceivedQuantity = receivedQuantity
		purchase.Price = float32(price)
		purchase.Total = float32(total)
		purchase.ReceiptNo = receiptNo
		purchase.Histories = histories
		purchase.Status = status
		purchase.UpdatedAt = time.Now()

		// Update purchase
		if err = i.purchaseRepo.Update(purchase); err != nil {
			return err
		}

		if updatedStock > 0 {
			// Add product stock
			if err = i.productService.AddStock(updatedStock, product.ID); err != nil {
				return err
			}
		}
	} else if err == errs.EntityNotFound {
		// Create purchase and update product stock
		purchase = &domain.PurchaseDomain{
			ProductID:        product.ID,
			Quantity:         quantity,
			ReceivedQuantity: receivedQuantity,
			Price:            float32(price),
			Total:            float32(total),
			ReceiptNo:        receiptNo,
			Histories:        histories,
			Status:           status,
			CreatedAt:        createdAt,
			UpdatedAt:        time.Now(),
		}

		// Create purchase
		if _, err = i.purchaseRepo.Create(purchase); err != nil {
			return err
		}

		if purchase.ReceivedQuantity > 0 {
			// Add product stock
			if err = i.productService.AddStock(purchase.ReceivedQuantity, product.ID); err != nil {
				return err
			}
		}
	} else {
		return err
	}

	return nil
}

func (i *PurchaseImporterCSV) parseHistories(notes string) ([]domain.ReceivedHistory, error) {
	histories := make([]domain.ReceivedHistory, 0)

	notesArr := strings.Split(notes, "; ")
	for _, nt := range notesArr {
		note := strings.ToLower(strings.TrimSpace(nt))
		if note != "masih menunggu" {
			noteArr := strings.Split(note, " terima ")
			receivedAt, err := time.Parse("2006/01/02", noteArr[0])
			if err != nil {
				receivedAt = time.Now()
			}
			quantity, err := strconv.Atoi(noteArr[1])
			if err != nil {
				quantity = 0
			}

			histories = append(histories, domain.ReceivedHistory{
				ReceivedAt: receivedAt,
				Quantity:   quantity,
			})
		}
	}

	return histories, nil
}
