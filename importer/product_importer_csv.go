package importer

import (
	"encoding/csv"
	"invenstore/errs"
	"invenstore/product/domain"
	"invenstore/product/repository"
	"io"
	"strconv"
)

type ProductImporterCSV struct {
	productRepo repository.ProductRepository
}

func NewProductImporterCSV(productRepo repository.ProductRepository) Importer {
	return &ProductImporterCSV{productRepo}
}

func (i *ProductImporterCSV) Import(reader io.Reader) error {
	r := csv.NewReader(reader)
	isHeader := true

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		if isHeader {
			isHeader = false
			continue
		}

		// Save product
		if err = i.save(record); err != nil {
			return err
		}
	}

	return nil
}

func (i *ProductImporterCSV) save(data []string) error {
	sku := data[0]
	name := data[1]
	stock, err := strconv.Atoi(data[2])
	if err != nil {
		return err
	}

	product, err := i.productRepo.FindBySKU(sku)
	if err == nil {
		// Update product if err is nil
		if err = product.Update(sku, name, stock); err != nil {
			return err
		}
		if err = i.productRepo.Update(product); err != nil {
			return err
		}
	} else if err == errs.EntityNotFound {
		// Create product if err is EntityNotFound
		product, err = domain.NewProduct(sku, name, stock)
		if err != nil {
			return err
		}
		if _, err = i.productRepo.Create(product); err != nil {
			return err
		}
	} else {
		return err
	}

	return nil
}
