package importer

import (
	"encoding/csv"
	"invenstore/errs"
	"invenstore/product/service"
	pr "invenstore/purchase/repository"
	"invenstore/sale/domain"
	"invenstore/sale/enum"
	"invenstore/sale/repository"
	"io"
	"strconv"
	"strings"
	"time"
)

type SaleImporterCSV struct {
	saleRepo       repository.SaleRepository
	purchaseRepo   pr.PurchaseRepository
	productService service.ProductService
}

func NewSaleImporterCSV(
	saleRepo repository.SaleRepository,
	purchaseRepo pr.PurchaseRepository,
	productService service.ProductService,
) Importer {
	return &SaleImporterCSV{saleRepo, purchaseRepo, productService}
}

func (i *SaleImporterCSV) Import(reader io.Reader) error {
	r := csv.NewReader(reader)
	isHeader := true

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}

		if isHeader {
			isHeader = false
			continue
		}

		// Save sale
		if err = i.save(record); err != nil {
			return err
		}
	}

	return nil
}

func (i *SaleImporterCSV) save(data []string) error {
	var createdAt time.Time
	var purchasePrice float32

	for _, df := range dateFormats() {
		t, err := time.Parse(df, data[0])
		if err == nil {
			createdAt = t
			break
		}
	}

	if createdAt.IsZero() {
		createdAt = time.Now()
	}

	sku := data[1]
	name := data[2]
	quantity, err := strconv.Atoi(data[3])
	if err != nil {
		quantity = 0
	}
	price, err := strconv.ParseFloat(data[4], 32)
	if err != nil {
		price = 0
	}
	total, err := strconv.ParseFloat(data[5], 32)
	if err != nil {
		total = 0
	}
	orderID, status := i.parseOrderIDAndStatus(data[6])

	product, err := i.productService.FindOrCreate(sku, name)
	if err != nil {
		return err
	}

	if status != enum.Ordered {
		price = 0
		total = 0
	} else {
		purchase, err := i.purchaseRepo.FindLatestByProductID(product.ID)
		if err == nil {
			purchasePrice = purchase.Price
		}
	}

	sale, err := i.saleRepo.FindByProductIDAndCreatedAt(product.ID, createdAt)
	if err == nil {
		// Calculate the old quantity and the new one
		itemQty := quantity - sale.Quantity

		sale.ProductID = product.ID
		sale.Quantity = quantity
		sale.Price = float32(price)
		sale.PurchasePrice = purchasePrice
		sale.Total = float32(total)
		sale.OrderID = orderID
		sale.Status = status
		sale.UpdatedAt = time.Now()

		if err = i.saleRepo.Update(sale); err != nil {
			return err
		}

		if itemQty > 0 {
			// Reduce the product stock
			if err = i.productService.ReduceStock(itemQty, product.ID); err != nil {
				return err
			}
		}
	} else if err == errs.EntityNotFound {
		sale = &domain.SaleDomain{
			ProductID:     product.ID,
			Quantity:      quantity,
			Price:         float32(price),
			PurchasePrice: purchasePrice,
			Total:         float32(total),
			OrderID:       orderID,
			Status:        status,
			CreatedAt:     createdAt,
			UpdatedAt:     time.Now(),
		}

		if _, err = i.saleRepo.Create(sale); err != nil {
			return err
		}

		if quantity > 0 {
			// Reduce the product stock
			if err = i.productService.ReduceStock(quantity, product.ID); err != nil {
				return err
			}
		}
	} else {
		return err
	}

	return nil
}

func (i *SaleImporterCSV) parseOrderIDAndStatus(notes string) (string, enum.Status) {
	var orderID string
	var status enum.Status

	switch nt := strings.ToLower(strings.TrimSpace(notes)); nt {
	case "barang rusak":
		status = enum.Broken
	case "barang hilang":
		status = enum.Missing
	case "sample barang":
		status = enum.Sample
	default:
		prefix := "pesanan "
		if strings.HasPrefix(nt, prefix) {
			orderID = strings.ToUpper(strings.TrimPrefix(nt, prefix))
		}

		status = enum.Ordered
	}

	return orderID, status
}
