package domain

import "time"

type Product struct {
	ID        uint      `json:"id"`
	SKU       string    `json:"sku"`
	Name      string    `json:"name"`
	Stock     int       `json:"stock"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func NewProduct(sku, name string, stock int) (*Product, error) {
	return &Product{
		SKU:       sku,
		Name:      name,
		Stock:     stock,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}, nil
}

func (d *Product) Update(sku, name string, stock int) error {
	d.SKU = sku
	d.Name = name
	d.Stock = stock
	d.UpdatedAt = time.Now()

	return nil
}

func (d *Product) AddStock(stock int) error {
	d.Stock += stock
	d.UpdatedAt = time.Now()

	return nil
}

func (d *Product) ReduceStock(stock int) error {
	d.Stock -= stock
	d.UpdatedAt = time.Now()

	return nil
}

func (d *Product) SetTimestamps(createdAt, updatedAt int64) {
	d.CreatedAt = time.Unix(createdAt, 0)
	d.UpdatedAt = time.Unix(updatedAt, 0)
}
