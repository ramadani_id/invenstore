package service

import "invenstore/product/domain"

type ProductService interface {
	Create(sku, name string, stock int) (*domain.Product, error)
	Update(sku, name string, stock int, id uint) (*domain.Product, error)
	Find(id uint) (*domain.Product, error)
	FindOrCreate(sku, name string) (*domain.Product, error)
	AddStock(stock int, id uint) error
	ReduceStock(stock int, id uint) error
	Paginate(page, perPage uint) ([]*domain.Product, uint, error)
}
