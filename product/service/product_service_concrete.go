package service

import (
	"invenstore/errs"
	"invenstore/product/domain"
	"invenstore/product/repository"
)

type ProductServiceConcrete struct {
	productRepo repository.ProductRepository
}

func NewProductServiceConcrete(productRepo repository.ProductRepository) ProductService {
	return &ProductServiceConcrete{productRepo}
}

func (s *ProductServiceConcrete) Create(sku, name string, stock int) (*domain.Product, error) {
	result := &domain.Product{}

	product, err := domain.NewProduct(sku, name, stock)
	if err != nil {
		return result, err
	}

	id, err := s.productRepo.Create(product)
	if err != nil {
		return result, err
	}

	return s.Find(id)
}

func (s *ProductServiceConcrete) Update(sku, name string, stock int, id uint) (*domain.Product, error) {
	result := &domain.Product{}

	product, err := s.productRepo.FindByID(id)
	if err != nil {
		return result, err
	}

	if err = product.Update(sku, name, stock); err != nil {
		return result, err
	}

	if err = s.productRepo.Update(product); err != nil {
		return result, err
	}

	return s.Find(id)
}

func (s *ProductServiceConcrete) Find(id uint) (*domain.Product, error) {
	result, err := s.productRepo.FindByID(id)
	if err != nil {
		return result, err
	}

	return result, nil
}

func (s *ProductServiceConcrete) FindOrCreate(sku, name string) (*domain.Product, error) {
	result, err := s.productRepo.FindBySKU(sku)
	if err != errs.EntityNotFound {
		return result, nil
	}

	// Create a new one
	return s.Create(sku, name, 0)
}

func (s *ProductServiceConcrete) AddStock(stock int, id uint) error {
	product, err := s.Find(id)
	if err != nil {
		return err
	}

	if err = product.AddStock(stock); err != nil {
		return err
	}

	if err = s.productRepo.Update(product); err != nil {
		return err
	}

	return nil
}

func (s *ProductServiceConcrete) ReduceStock(stock int, id uint) error {
	product, err := s.Find(id)
	if err != nil {
		return err
	}

	if err = product.ReduceStock(stock); err != nil {
		return err
	}

	if err = s.productRepo.Update(product); err != nil {
		return err
	}

	return nil
}

func (s *ProductServiceConcrete) Paginate(page, perPage uint) ([]*domain.Product, uint, error) {
	var total uint
	offset := (page - 1) * perPage

	products, err := s.productRepo.Fetch(offset, perPage)
	if err != nil {
		return products, total, err
	}

	total, err = s.productRepo.Total()
	if err != nil {
		return products, total, err
	}

	return products, total, nil
}
