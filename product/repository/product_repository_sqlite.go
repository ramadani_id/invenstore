package repository

import (
	"database/sql"
	"invenstore/errs"
	"invenstore/product/domain"
)

type ProductRepositorySQLite struct {
	db *sql.DB
}

const (
	CreateQuery    = "INSERT INTO products (sku, name, stock, created_at, updated_at) VALUES (?, ?, ?, ?, ?)"
	UpdateQuery    = "UPDATE products SET sku = ?, name = ?, stock = ?, updated_at = ? WHERE id = ?"
	FindByIDQuery  = "SELECT id, sku, name, stock, created_at, updated_at FROM products WHERE id = ?"
	FindBySKUQuery = "SELECT id, sku, name, stock, created_at, updated_at FROM products WHERE sku = ?"
	FetchQuery     = "SELECT id, sku, name, stock, created_at, updated_at FROM products ORDER BY created_at DESC LIMIT ?, ?"
	TotalQuery     = "SELECT COUNT(id) as total FROM products"
)

func NewProductRepositorySQLite(db *sql.DB) ProductRepository {
	return &ProductRepositorySQLite{db}
}

func (r *ProductRepositorySQLite) Create(product *domain.Product) (uint, error) {
	var id uint

	stmt, err := r.db.Prepare(CreateQuery)
	if err != nil {
		return id, err
	}
	defer stmt.Close()

	result, err := stmt.Exec(product.SKU, product.Name, product.Stock,
		product.CreatedAt.Unix(), product.UpdatedAt.Unix())
	if err != nil {
		return id, err
	}

	lastID, err := result.LastInsertId()
	if err != nil {
		return id, err
	}

	id = uint(lastID)

	return id, nil
}

func (r *ProductRepositorySQLite) Update(product *domain.Product) error {
	stmt, err := r.db.Prepare(UpdateQuery)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(product.SKU, product.Name, product.Stock, product.UpdatedAt.Unix(), product.ID)
	if err != nil {
		return err
	}

	return nil
}

func (r *ProductRepositorySQLite) FindByID(id uint) (*domain.Product, error) {
	result := &domain.Product{}
	var createdAt int64
	var updatedAt int64

	row := r.db.QueryRow(FindByIDQuery, id)
	err := row.Scan(&result.ID, &result.SKU, &result.Name, &result.Stock, &createdAt, &updatedAt)
	if err != nil && err == sql.ErrNoRows {
		return result, errs.EntityNotFound
	} else if err != nil {
		return result, err
	}

	result.SetTimestamps(createdAt, updatedAt)

	return result, nil
}

func (r *ProductRepositorySQLite) FindBySKU(sku string) (*domain.Product, error) {
	result := &domain.Product{}
	var createdAt int64
	var updatedAt int64

	row := r.db.QueryRow(FindBySKUQuery, sku)
	err := row.Scan(&result.ID, &result.SKU, &result.Name, &result.Stock, &createdAt, &updatedAt)
	if err != nil && err == sql.ErrNoRows {
		return result, errs.EntityNotFound
	} else if err != nil {
		return result, err
	}

	result.SetTimestamps(createdAt, updatedAt)

	return result, nil
}

func (r *ProductRepositorySQLite) Fetch(offset, limit uint) ([]*domain.Product, error) {
	results := make([]*domain.Product, 0)

	rows, err := r.db.Query(FetchQuery, offset, limit)
	if err != nil {
		return results, err
	}
	defer rows.Close()

	for rows.Next() {
		product := &domain.Product{}
		var createdAt int64
		var updatedAt int64

		err := rows.Scan(&product.ID, &product.SKU, &product.Name, &product.Stock,
			&createdAt, &updatedAt)
		if err != nil {
			return results, err
		}

		product.SetTimestamps(createdAt, updatedAt)

		results = append(results, product)
	}

	return results, nil
}

func (r *ProductRepositorySQLite) Total() (uint, error) {
	var total uint

	if err := r.db.QueryRow(TotalQuery).Scan(&total); err != nil {
		return total, err
	}

	return total, nil
}
