package repository

import "invenstore/product/domain"

type ProductRepository interface {
	Create(product *domain.Product) (uint, error)
	Update(product *domain.Product) error
	FindByID(id uint) (*domain.Product, error)
	FindBySKU(sku string) (*domain.Product, error)
	Fetch(offset, limit uint) ([]*domain.Product, error)
	Total() (uint, error)
}
