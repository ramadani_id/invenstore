package csv

import (
	"bytes"
	"encoding/csv"
)

func BufferWriter(contents [][]string) (*bytes.Buffer, error) {
	b := &bytes.Buffer{}
	w := csv.NewWriter(b)

	if err := w.WriteAll(contents); err != nil {
		return b, err
	}
	w.Flush()

	if err := w.Error(); err != nil {
		return b, err
	}

	return b, nil
}
