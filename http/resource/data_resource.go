package resource

func NewDataResource(data interface{}) interface{} {
	result := make(map[string]interface{})
	result["data"] = data

	return result
}
