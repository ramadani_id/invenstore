package resource

import "math"

type PaginationResource struct {
	Data interface{}     `json:"data"`
	Meta *MetaPagination `json:"meta"`
}

type MetaPagination struct {
	Page      uint `json:"page"`
	PerPage   uint `json:"per_page"`
	Total     uint `json:"total"`
	TotalPage uint `json:"total_pages"`
}

func NewPaginationResource(data interface{}, page, perPage, total uint) *PaginationResource {
	pages := float64(total) / float64(perPage)
	totalPages := uint(math.Ceil(pages))

	return &PaginationResource{
		Data: data,
		Meta: &MetaPagination{
			Page:      page,
			PerPage:   perPage,
			Total:     total,
			TotalPage: totalPages,
		},
	}
}
