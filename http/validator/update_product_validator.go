package validator

import (
	"errors"
	"invenstore/http/request"
	"invenstore/product/repository"
	"net/url"
)

type UpdateProductValidator struct {
	productRepo repository.ProductRepository
}

func NewUpdateProductValidator(productRepo repository.ProductRepository) Validator {
	return &UpdateProductValidator{productRepo}
}

func (v *UpdateProductValidator) Validate(input interface{}) error {
	data, ok := input.(*request.ProductRequest)
	if !ok {
		return errors.New("Can not convert to request")
	}

	errs := url.Values{}

	if data.Name == "" {
		errs.Add("name", "required")
	}
	if data.SKU == "" {
		errs.Add("sku", "required")
	}

	if data.SKU != "" {
		if p, _ := v.productRepo.FindBySKU(data.SKU); p.SKU == data.SKU && p.ID != data.ID {
			errs.Add("sku", "unique")
		}
	}

	if len(errs) > 0 {
		return &ValidationError{FieldErrors: errs}
	}

	return nil
}
