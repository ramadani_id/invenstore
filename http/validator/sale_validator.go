package validator

import (
	"errors"
	"invenstore/sale/input"
	"net/url"
)

type SaleValidator struct{}

func NewSaleValidator() Validator {
	return &SaleValidator{}
}

func (v *SaleValidator) Validate(value interface{}) error {
	data, ok := value.(*input.SaleInput)
	if !ok {
		return errors.New("Can not convert to request")
	}

	errs := url.Values{}

	if data.ProductID == 0 {
		errs.Add("product_id", "required")
	}
	if data.Quantity == 0 {
		errs.Add("quantity", "min:1")
	}
	if data.Price == 0 {
		errs.Add("price", "min:1")
	}

	if len(errs) > 0 {
		return &ValidationError{FieldErrors: errs}
	}

	return nil
}
