package validator

import (
	"errors"
	"invenstore/http/request"
	"net/url"
)

type PurchasesReceivedHistoryValidator struct{}

func NewPurchasesReceivedHistoryValidator() Validator {
	return &PurchasesReceivedHistoryValidator{}
}

func (v *PurchasesReceivedHistoryValidator) Validate(input interface{}) error {
	data, ok := input.(*request.PurchasesReceivedHistoryRequest)
	if !ok {
		return errors.New("Can not convert to request")
	}

	errs := url.Values{}

	if data.ReceivedAt.Time.IsZero() {
		errs.Add("received_at", "required")
	}
	if data.Quantity == 0 {
		errs.Add("quantity", "required")
	}

	if len(errs) > 0 {
		return &ValidationError{FieldErrors: errs}
	}

	return nil
}
