package validator

import (
	"errors"
	"invenstore/purchase/input"
	"net/url"
)

type PurchaseValidator struct{}

func NewPurchaseValidator() Validator {
	return &PurchaseValidator{}
}

func (v *PurchaseValidator) Validate(value interface{}) error {
	data, ok := value.(*input.ProductPurchaseInput)
	if !ok {
		return errors.New("Can not convert to request")
	}

	errs := url.Values{}

	if data.SKU == "" {
		errs.Add("sku", "required")
	}
	if data.Quantity == 0 {
		errs.Add("quantity", "min:1")
	}
	if data.Price == 0 {
		errs.Add("price", "min:1")
	}

	if len(errs) > 0 {
		return &ValidationError{FieldErrors: errs}
	}

	return nil
}
