package validator

import "net/url"

type Validator interface {
	Validate(input interface{}) error
}

type ValidationError struct {
	FieldErrors url.Values
}

func (v *ValidationError) Error() string {
	return "validation_error"
}

func (v *ValidationError) Errors() url.Values {
	return v.FieldErrors
}
