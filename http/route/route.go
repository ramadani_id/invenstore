package route

import (
	"invenstore/http/controller"
	"net/http"

	"github.com/labstack/echo"
)

type Controller struct {
	Product  controller.ProductController
	Purchase controller.PurchaseController
	Sale     controller.SaleController
	Report   controller.ReportController
}

func Route(e *echo.Echo, ct *Controller) {
	e.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, map[string]string{
			"name":    "Invenstore API",
			"version": "v1.0.0",
		})
	})

	e.GET("/products", ct.Product.Index)
	e.POST("/products", ct.Product.Create)
	e.GET("/products/:id", ct.Product.Find)
	e.PUT("/products/:id", ct.Product.Update)
	e.POST("/products/import", ct.Product.Import)

	e.GET("/purchases", ct.Purchase.Index)
	e.POST("/purchases", ct.Purchase.Create)
	e.GET("/purchases/:id", ct.Purchase.Find)
	e.PUT("/purchases/:id", ct.Purchase.Update)
	e.POST("/purchases/:id/history", ct.Purchase.AddHistory)
	e.POST("/purchases/import", ct.Purchase.Import)

	e.GET("/sales", ct.Sale.Index)
	e.POST("/sales", ct.Sale.Create)
	e.GET("/sales/:id", ct.Sale.Find)
	e.PUT("/sales/:id/broken", ct.Sale.SetBroken)
	e.PUT("/sales/:id/missing", ct.Sale.SetMissing)
	e.POST("/sales/import", ct.Sale.Import)

	e.GET("/inventory_report", ct.Report.Inventory)
	e.GET("/sales_report", ct.Report.Sales)
}
