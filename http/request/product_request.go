package request

type ProductRequest struct {
	ID    uint
	SKU   string `json:"sku"`
	Name  string `json:"name"`
	Stock int    `json:"stock"`
}
