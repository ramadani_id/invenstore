package request

type PurchasesReceivedHistoryRequest struct {
	ReceivedAt Date `json:"received_at"`
	Quantity   int  `json:"quantity"`
}
