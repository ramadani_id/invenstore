package request

import (
	"encoding/json"
	"time"
)

type Date struct {
	Time time.Time
}

func (d *Date) UnmarshalJSON(data []byte) error {
	var date string
	if err := json.Unmarshal(data, &date); err != nil {
		return err
	}

	t, err := time.Parse("2006-01-02", date)
	if err != nil {
		return err
	}

	d.Time = t

	return nil
}
