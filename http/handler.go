package http

import (
	"fmt"
	"invenstore/errs"
	"invenstore/http/validator"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"github.com/labstack/echo"
)

type appErrs struct {
	Errors []*appErr `json:"errors"`
}

type appErr struct {
	Type    string     `json:"type"`
	Details []string   `json:"details,omitempty"`
	Errors  url.Values `json:"errors,omitempty"`
}

func HTTPErrorHandler(e *echo.Echo) func(err error, c echo.Context) {
	return func(err error, c echo.Context) {
		appErrs := &appErrs{
			Errors: make([]*appErr, 0),
		}

		var (
			code = http.StatusInternalServerError
			msg  interface{}
		)

		if he, ok := err.(*echo.HTTPError); ok {
			code = he.Code
			msg = he.Message
			if he.Internal != nil {
				err = fmt.Errorf("%v, %v", err, he.Internal)
			}
		} else if err == errs.EntityNotFound {
			code = http.StatusNotFound
			msg = err.Error()
		} else if err, ok := err.(*validator.ValidationError); ok {
			code = http.StatusUnprocessableEntity

			appErrs.Errors = append(appErrs.Errors, &appErr{
				Type:   err.Error(),
				Errors: err.Errors(),
			})
		} else if e.Debug {
			msg = err.Error()
		} else {
			msg = http.StatusText(code)
		}

		if msgStr, ok := msg.(string); ok {
			appErrs.Errors = append(appErrs.Errors, &appErr{
				Type:    toSnakeCase(http.StatusText(code)),
				Details: []string{msgStr},
			})
		}

		// Send response
		if !c.Response().Committed {
			if c.Request().Method == http.MethodHead {
				err = c.NoContent(code)
			} else {
				err = c.JSON(code, appErrs)
			}
			if err != nil {
				e.Logger.Error(err)
			}
		}
	}
}

var matchFirstCap = regexp.MustCompile("(.)([A-Z][a-z]+)")
var matchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")

func toSnakeCase(str string) string {
	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = matchAllCap.ReplaceAllString(snake, "${1}_${2}")
	return strings.ReplaceAll(strings.ToLower(snake), " ", "")
}
