package controller

import "github.com/labstack/echo"

type ProductController interface {
	Index(c echo.Context) error
	Create(c echo.Context) error
	Find(c echo.Context) error
	Update(c echo.Context) error
	Import(c echo.Context) error
}
