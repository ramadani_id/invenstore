package concrete

import (
	"invenstore/http/controller"
	"invenstore/http/request"
	"invenstore/http/resource"
	"invenstore/http/validator"
	"invenstore/importer"
	"invenstore/product/service"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

type ProductControllerConcrete struct {
	createProductValidator validator.Validator
	updateProductValidator validator.Validator
	productService         service.ProductService
	productImporter        importer.Importer
}

func NewProductControllerConcrete(
	createProductValidator validator.Validator,
	updateProductValidator validator.Validator,
	productService service.ProductService,
	productImporter importer.Importer,
) controller.ProductController {
	return &ProductControllerConcrete{
		createProductValidator,
		updateProductValidator,
		productService,
		productImporter,
	}
}

func (ct *ProductControllerConcrete) Index(c echo.Context) error {
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		page = 1
	}

	perPage, err := strconv.Atoi(c.QueryParam("per_page"))
	if err != nil {
		perPage = 10
	}

	products, total, err := ct.productService.Paginate(uint(page), uint(perPage))
	if err != nil {
		return err
	}

	result := resource.NewPaginationResource(products, uint(page), uint(perPage), total)

	return c.JSON(http.StatusOK, result)
}

func (ct *ProductControllerConcrete) Create(c echo.Context) error {
	input := &request.ProductRequest{}
	if err := c.Bind(input); err != nil {
		return err
	}

	// Validation Fields
	if err := ct.createProductValidator.Validate(input); err != nil {
		return err
	}

	// Create product
	result, err := ct.productService.Create(input.SKU, input.Name, input.Stock)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, resource.NewDataResource(result))
}

func (ct *ProductControllerConcrete) Find(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	result, err := ct.productService.Find(uint(id))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, resource.NewDataResource(result))
}

func (ct *ProductControllerConcrete) Update(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	input := &request.ProductRequest{}
	if err := c.Bind(input); err != nil {
		return err
	}
	input.ID = uint(id)

	// Validation Fields
	if err := ct.updateProductValidator.Validate(input); err != nil {
		return err
	}

	// Update product
	result, err := ct.productService.Update(input.SKU, input.Name, input.Stock, uint(id))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, resource.NewDataResource(result))
}

func (ct *ProductControllerConcrete) Import(c echo.Context) error {
	file, err := c.FormFile("file")
	if err != nil {
		return err
	}
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	// Import from file
	if err = ct.productImporter.Import(src); err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}
