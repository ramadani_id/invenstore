package concrete

import (
	"invenstore/http/controller"
	"invenstore/http/request"
	"invenstore/http/resource"
	"invenstore/http/validator"
	"invenstore/importer"
	"invenstore/purchase/input"
	"invenstore/purchase/service"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

type PurchaseControllerConcrete struct {
	purchaseValidator        validator.Validator
	receivedHistoryValidator validator.Validator
	purchaseService          service.PurchaseService
	purchaseImporter         importer.Importer
}

func NewPurchaseControllerConcrete(
	purchaseValidator validator.Validator,
	receivedHistoryValidator validator.Validator,
	purchaseService service.PurchaseService,
	purchaseImporter importer.Importer,
) controller.PurchaseController {
	return &PurchaseControllerConcrete{
		purchaseValidator,
		receivedHistoryValidator,
		purchaseService,
		purchaseImporter,
	}
}

func (ct *PurchaseControllerConcrete) Index(c echo.Context) error {
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		page = 1
	}

	perPage, err := strconv.Atoi(c.QueryParam("per_page"))
	if err != nil {
		perPage = 10
	}

	purchases, total, err := ct.purchaseService.Paginate(uint(page), uint(perPage))
	if err != nil {
		return err
	}

	result := resource.NewPaginationResource(purchases, uint(page), uint(perPage), total)

	return c.JSON(http.StatusOK, result)
}

func (ct *PurchaseControllerConcrete) Create(c echo.Context) error {
	input := &input.ProductPurchaseInput{}
	if err := c.Bind(input); err != nil {
		return err
	}

	// Validation Fields
	if err := ct.purchaseValidator.Validate(input); err != nil {
		return err
	}

	// Create purchase
	result, err := ct.purchaseService.Create(input)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, resource.NewDataResource(result))
}

func (ct *PurchaseControllerConcrete) Find(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	result, err := ct.purchaseService.Find(uint(id))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, resource.NewDataResource(result))
}

func (ct *PurchaseControllerConcrete) Update(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	input := &input.ProductPurchaseInput{}
	if err := c.Bind(input); err != nil {
		return err
	}

	// Validation Fields
	if err := ct.purchaseValidator.Validate(input); err != nil {
		return err
	}

	// Update purchase
	result, err := ct.purchaseService.Update(input, uint(id))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, resource.NewDataResource(result))
}

func (ct *PurchaseControllerConcrete) AddHistory(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	input := &request.PurchasesReceivedHistoryRequest{}
	if err := c.Bind(input); err != nil {
		return err
	}

	// Validation Fields
	if err := ct.receivedHistoryValidator.Validate(input); err != nil {
		return err
	}

	// Add received history
	result, err := ct.purchaseService.AddHistory(input.ReceivedAt.Time, input.Quantity, uint(id))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, resource.NewDataResource(result))
}

func (ct *PurchaseControllerConcrete) Import(c echo.Context) error {
	file, err := c.FormFile("file")
	if err != nil {
		return err
	}
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	// Import from file
	if err = ct.purchaseImporter.Import(src); err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}
