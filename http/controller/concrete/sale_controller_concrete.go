package concrete

import (
	"invenstore/http/controller"
	"invenstore/http/resource"
	"invenstore/http/validator"
	"invenstore/importer"
	"invenstore/sale/input"
	"invenstore/sale/service"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

type SaleControllerConcrete struct {
	saleValidator validator.Validator
	saleService   service.SaleService
	saleImporter  importer.Importer
}

func NewSaleControllerConcrete(
	saleValidator validator.Validator,
	saleService service.SaleService,
	saleImporter importer.Importer,
) controller.SaleController {
	return &SaleControllerConcrete{saleValidator, saleService, saleImporter}
}

func (ct *SaleControllerConcrete) Index(c echo.Context) error {
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		page = 1
	}

	perPage, err := strconv.Atoi(c.QueryParam("per_page"))
	if err != nil {
		perPage = 10
	}

	purchases, total, err := ct.saleService.Paginate(uint(page), uint(perPage))
	if err != nil {
		return err
	}

	result := resource.NewPaginationResource(purchases, uint(page), uint(perPage), total)

	return c.JSON(http.StatusOK, result)
}

func (ct *SaleControllerConcrete) Create(c echo.Context) error {
	input := &input.SaleInput{}
	if err := c.Bind(input); err != nil {
		return err
	}

	// Validation Fields
	if err := ct.saleValidator.Validate(input); err != nil {
		return err
	}

	// Create sale
	result, err := ct.saleService.Create(input)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, resource.NewDataResource(result))
}

func (ct *SaleControllerConcrete) Find(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	result, err := ct.saleService.Find(uint(id))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, resource.NewDataResource(result))
}

func (ct *SaleControllerConcrete) SetBroken(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	result, err := ct.saleService.SetBroken(uint(id))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, resource.NewDataResource(result))
}

func (ct *SaleControllerConcrete) SetMissing(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}

	result, err := ct.saleService.SetMissing(uint(id))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, resource.NewDataResource(result))
}

func (ct *SaleControllerConcrete) Import(c echo.Context) error {
	file, err := c.FormFile("file")
	if err != nil {
		return err
	}
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	// Import from file
	if err = ct.saleImporter.Import(src); err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}
