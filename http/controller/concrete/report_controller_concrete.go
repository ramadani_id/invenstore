package concrete

import (
	"fmt"
	"invenstore/csv"
	"invenstore/http/controller"
	"invenstore/report/service"
	"net/http"
	"time"

	"github.com/labstack/echo"
)

type ReportControllerConcrete struct {
	reportService service.ReportService
}

func NewReportControllerConcrete(reportService service.ReportService) controller.ReportController {
	return &ReportControllerConcrete{reportService}
}

func (ct *ReportControllerConcrete) Inventory(c echo.Context) error {
	report, err := ct.reportService.InventoryReportFile()
	if err != nil {
		return err
	}

	contents, err := report.Content()
	if err != nil {
		return err
	}

	b, err := csv.BufferWriter(contents)
	if err != nil {
		return err
	}

	c.Response().Header().Set("Content-Description", "File Transfer")
	c.Response().Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s", report.Filename()))

	return c.Blob(http.StatusOK, "text/csv", b.Bytes())
}

func (ct *ReportControllerConcrete) Sales(c echo.Context) error {
	startAt, err := time.Parse("2006-01-02", c.QueryParam("start_date"))
	if err != nil {
		startAt = time.Now()
	}
	endAt, err := time.Parse("2006-01-02", c.QueryParam("end_date"))
	if err != nil {
		endAt = time.Now()
	}

	report, err := ct.reportService.SalesReportFile(startAt, endAt)
	if err != nil {
		return err
	}

	contents, err := report.Content()
	if err != nil {
		return err
	}

	b, err := csv.BufferWriter(contents)
	if err != nil {
		return err
	}

	c.Response().Header().Set("Content-Description", "File Transfer")
	c.Response().Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s", report.Filename()))

	return c.Blob(http.StatusOK, "text/csv", b.Bytes())
}
