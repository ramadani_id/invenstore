package controller

import "github.com/labstack/echo"

type PurchaseController interface {
	Index(c echo.Context) error
	Create(c echo.Context) error
	Find(c echo.Context) error
	Update(c echo.Context) error
	AddHistory(c echo.Context) error
	Import(c echo.Context) error
}
