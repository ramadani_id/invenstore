package controller

import "github.com/labstack/echo"

type ReportController interface {
	Inventory(c echo.Context) error
	Sales(c echo.Context) error
}
