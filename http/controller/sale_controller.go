package controller

import "github.com/labstack/echo"

type SaleController interface {
	Index(c echo.Context) error
	Create(c echo.Context) error
	Find(c echo.Context) error
	SetBroken(c echo.Context) error
	SetMissing(c echo.Context) error
	Import(c echo.Context) error
}
