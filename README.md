# INVENSTORE

Inventory Management System

## Requirements

* Go >= 1.10
* dep - Go dependency management tool

## Dependencies

* [echo](https://github.com/labstack/echo)
* [go-sqlite3](https://github.com/mattn/go-sqlite3)

## Installation

1. Make sure `go` and `dep` have been installed on your machine
2. Just clone this repository
3. Run the `dep ensure` command to install the required dependencies

## Running

Run the API by using this command:

```bash
$ go run main.go
```

You can run the API with optional arguments

```bash
$ go run main.go -address=:4000 -dbname=invenstore.db
```

Or you can use the invenstore binary from bin directory. Example:

```bash
$ bin/macos/64/invenstore
```

```bash
$ bin/macos/64/invenstore -address=:4000 -dbname=invenstore.db
```

Check the API by running:

```bash
$ curl localhost:4000
{"name":"Invenstore API","version":"v1.0.0"}
```

## API Documentation

You can see the endpoints on this API in the [Postman API Documenter](https://documenter.getpostman.com/view/8210863/SVSPmmCf), or you can also import them into your Postman that is available in the `doc` directory.

## Assumption

1. The API Response is only raw data, to format the data as in the "Catatan" column in the "Catatan Barang Masuk" and "Catatan Barang Keluar" sheet will be done on the presentation (UI) layer.

2. Adding data to the "Catatan Barang Masuk" can add a new product because this feature can be a gateway to creating new products that are not in the records.

3. The product stock will be updated when the product has been received by creating a history of product purchases that have been received.

4. To import products, purchases, and sales, please use the same format on the CSV sample files that are available in the `doc` directory.