package container

import (
	"database/sql"
	"invenstore/http/controller"
	"invenstore/http/validator"
	"invenstore/importer"
	"invenstore/migration"
	pr "invenstore/product/repository"
	ps "invenstore/product/service"
	pur "invenstore/purchase/repository"
	pus "invenstore/purchase/service"
	rr "invenstore/report/repository"
	rs "invenstore/report/service"
	sr "invenstore/sale/repository"
	ss "invenstore/sale/service"
)

type Container interface {
	DbDsn(dbname string) (string, error)
	Migration(db *sql.DB) migration.Migration

	ProductRepository(db *sql.DB) pr.ProductRepository
	ProductService(productRepo pr.ProductRepository) ps.ProductService
	ProductImporter(productRepo pr.ProductRepository) importer.Importer

	PurchaseRepository(db *sql.DB) pur.PurchaseRepository
	PurchaseService(productService ps.ProductService, purchaseRepo pur.PurchaseRepository) pus.PurchaseService
	PurchaseImporter(productService ps.ProductService, purchaseRepo pur.PurchaseRepository) importer.Importer

	SaleRepository(db *sql.DB) sr.SaleRepository
	SaleService(saleRepo sr.SaleRepository, purchaseRepo pur.PurchaseRepository, productService ps.ProductService) ss.SaleService
	SaleImporter(saleRepo sr.SaleRepository, purchaseRepo pur.PurchaseRepository, productService ps.ProductService) importer.Importer

	ReportRepository(db *sql.DB) rr.ReportRepository
	ReportService(reportRepo rr.ReportRepository) rs.ReportService

	ProductController(
		createProductValidator validator.Validator,
		updateProductValidator validator.Validator,
		productService ps.ProductService,
		productImporter importer.Importer,
	) controller.ProductController
	PurchaseController(
		purchaseValidator validator.Validator,
		receivedHistoryValidator validator.Validator,
		purchaseService pus.PurchaseService,
		purchaseImporter importer.Importer,
	) controller.PurchaseController
	SaleController(
		saleValidator validator.Validator,
		saleService ss.SaleService,
		saleImporter importer.Importer,
	) controller.SaleController
	ReportController(reportService rs.ReportService) controller.ReportController
}
