package container

import (
	"database/sql"
	"invenstore/http/controller"
	"invenstore/http/controller/concrete"
	"invenstore/http/validator"
	"invenstore/importer"
	"invenstore/migration"
	pr "invenstore/product/repository"
	ps "invenstore/product/service"
	pur "invenstore/purchase/repository"
	pus "invenstore/purchase/service"
	rr "invenstore/report/repository"
	rs "invenstore/report/service"
	sr "invenstore/sale/repository"
	ss "invenstore/sale/service"
	"os"
)

type kernel struct{}

func ServiceContainer() Container {
	return &kernel{}
}

func (k *kernel) DbDsn(dbname string) (string, error) {
	filename := dbname
	_, err := os.Stat(filename)
	if os.IsNotExist(err) {
		_, err = os.Create(filename)
		if err != nil {
			return "", err
		}
	}

	return "./" + filename, nil
}

func (k *kernel) Migration(db *sql.DB) migration.Migration {
	return migration.NewSQLiteMigration(db)
}

func (k *kernel) ProductRepository(db *sql.DB) pr.ProductRepository {
	return pr.NewProductRepositorySQLite(db)
}

func (k *kernel) ProductService(productRepo pr.ProductRepository) ps.ProductService {
	return ps.NewProductServiceConcrete(productRepo)
}

func (k *kernel) ProductImporter(productRepo pr.ProductRepository) importer.Importer {
	return importer.NewProductImporterCSV(productRepo)
}

func (k *kernel) PurchaseRepository(db *sql.DB) pur.PurchaseRepository {
	return pur.NewPurchaseRepositorySQLite(db)
}

func (k *kernel) PurchaseService(productService ps.ProductService, purchaseRepo pur.PurchaseRepository) pus.PurchaseService {
	return pus.NewPurchaseServiceConcrete(productService, purchaseRepo)
}

func (k *kernel) PurchaseImporter(productService ps.ProductService, purchaseRepo pur.PurchaseRepository) importer.Importer {
	return importer.NewPurchaseImporterCSV(purchaseRepo, productService)
}

func (k *kernel) SaleRepository(db *sql.DB) sr.SaleRepository {
	return sr.NewSaleRepositorySQLite(db)
}

func (k *kernel) SaleService(
	saleRepo sr.SaleRepository,
	purchaseRepo pur.PurchaseRepository,
	productService ps.ProductService,
) ss.SaleService {
	return ss.NewSaleServiceConcrete(saleRepo, purchaseRepo, productService)
}

func (k *kernel) SaleImporter(
	saleRepo sr.SaleRepository,
	purchaseRepo pur.PurchaseRepository,
	productService ps.ProductService,
) importer.Importer {
	return importer.NewSaleImporterCSV(saleRepo, purchaseRepo, productService)
}

func (k *kernel) ReportRepository(db *sql.DB) rr.ReportRepository {
	return rr.NewReportRepositorySQLite(db)
}

func (k *kernel) ReportService(reportRepo rr.ReportRepository) rs.ReportService {
	return rs.NewReportServiceConcrete(reportRepo)
}

func (k *kernel) ProductController(
	createProductValidator validator.Validator,
	updateProductValidator validator.Validator,
	productService ps.ProductService,
	productImporter importer.Importer,
) controller.ProductController {
	return concrete.NewProductControllerConcrete(
		createProductValidator,
		updateProductValidator,
		productService,
		productImporter,
	)
}

func (k *kernel) PurchaseController(
	purchaseValidator validator.Validator,
	receivedHistoryValidator validator.Validator,
	purchaseService pus.PurchaseService,
	purchaseImporter importer.Importer,
) controller.PurchaseController {
	return concrete.NewPurchaseControllerConcrete(
		purchaseValidator,
		receivedHistoryValidator,
		purchaseService,
		purchaseImporter,
	)
}

func (k *kernel) SaleController(
	saleValidator validator.Validator,
	saleService ss.SaleService,
	saleImporter importer.Importer,
) controller.SaleController {
	return concrete.NewSaleControllerConcrete(saleValidator, saleService, saleImporter)
}

func (k *kernel) ReportController(reportService rs.ReportService) controller.ReportController {
	return concrete.NewReportControllerConcrete(reportService)
}
