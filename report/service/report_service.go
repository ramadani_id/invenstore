package service

import (
	"invenstore/report/reportfile"
	"time"
)

type ReportService interface {
	InventoryReportFile() (reportfile.ReportFile, error)
	SalesReportFile(startAt, endAt time.Time) (reportfile.ReportFile, error)
}
