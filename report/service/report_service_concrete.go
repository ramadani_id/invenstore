package service

import (
	"invenstore/report/reportfile"
	"invenstore/report/repository"
	"time"
)

type ReportServiceConcrete struct {
	reportRepo repository.ReportRepository
}

func NewReportServiceConcrete(reportRepo repository.ReportRepository) ReportService {
	return &ReportServiceConcrete{reportRepo}
}

func (s *ReportServiceConcrete) InventoryReportFile() (reportfile.ReportFile, error) {
	data, err := s.reportRepo.InventoryReport()
	if err != nil {
		return nil, err
	}

	return reportfile.NewInventoryReportFile(data), nil
}

func (s *ReportServiceConcrete) SalesReportFile(startAt, endAt time.Time) (reportfile.ReportFile, error) {
	data, err := s.reportRepo.SalesReport(startAt, endAt)
	if err != nil {
		return nil, err
	}

	return reportfile.NewSalesReportFile(startAt, endAt, data), nil
}
