package domain

type InventoryDomain struct {
	ID               uint
	SKU              string
	Name             string
	Stock            int
	PurchasePriceAvg float32
}

func (d *InventoryDomain) Total() float32 {
	return float32(d.Stock) * d.PurchasePriceAvg
}
