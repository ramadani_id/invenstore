package domain

import "time"

type SalesDomain struct {
	ID            uint
	OrderID       string
	SKU           string
	Name          string
	Quantity      int
	Price         float32
	PurchasePrice float32
	Total         float32
	CreatedAt     time.Time
}

func (d *SalesDomain) Profit() float32 {
	return d.Total - (float32(d.Quantity) * d.PurchasePrice)
}
