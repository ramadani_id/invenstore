package reportfile

type ReportFile interface {
	Filename() string
	Content() ([][]string, error)
}
