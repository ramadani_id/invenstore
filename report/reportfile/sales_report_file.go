package reportfile

import (
	"fmt"
	"invenstore/report/domain"
	"time"
)

type SalesReportFile struct {
	startAt, endAt time.Time
	data           []*domain.SalesDomain
}

func NewSalesReportFile(startAt, endAt time.Time, data []*domain.SalesDomain) ReportFile {
	return &SalesReportFile{startAt, endAt, data}
}

func (f *SalesReportFile) Filename() string {
	date := time.Now().Format("2006-01-02")
	return "sales_report_" + date + ".csv"
}

func (f *SalesReportFile) Content() ([][]string, error) {
	var totalItem int
	var total, totalProfit float32
	orderList := make(map[string]string)

	headerLength := 10
	length := headerLength + len(f.data)
	result := make([][]string, length)

	date := time.Now().Format("02 January 2006")
	startDate := f.startAt.Format("02 January 2006")
	endDate := f.endAt.Format("02 January 2006")

	result[0] = []string{"LAPORAN PENJUALAN"}
	result[1] = []string{}
	result[2] = []string{"Tanggal Cetak", date}
	result[3] = []string{"Tanggal", startDate + " - " + endDate}
	result[8] = []string{}
	result[9] = []string{"ID Pesanan", "Waktu", "SKU", "Nama Barang", "Jumlah", "Harga Jual", "Total", "Harga Beli", "Laba"}

	for i, d := range f.data {
		totalItem += d.Quantity
		total += d.Total
		totalProfit += d.Profit()
		orderList[d.OrderID] = d.OrderID

		result[i+headerLength] = []string{
			d.OrderID,
			d.CreatedAt.Format("2006-01-02 15:04:05"),
			d.SKU,
			d.Name,
			fmt.Sprintf("%d", d.Quantity),
			fmt.Sprintf("%.0f", d.Price),
			fmt.Sprintf("%.0f", d.Total),
			fmt.Sprintf("%.0f", d.PurchasePrice),
			fmt.Sprintf("%.0f", d.Profit()),
		}
	}

	result[4] = []string{"Total Omzet", fmt.Sprintf("%.0f", total)}
	result[5] = []string{"Total Laba Kotor", fmt.Sprintf("%.0f", totalProfit)}
	result[6] = []string{"Total Penjualan", fmt.Sprintf("%d", len(orderList))}
	result[7] = []string{"Total Barang", fmt.Sprintf("%d", totalItem)}

	return result, nil
}
