package reportfile

import (
	"fmt"
	"invenstore/report/domain"
	"time"
)

type InventoryReportFile struct {
	data []*domain.InventoryDomain
}

func NewInventoryReportFile(data []*domain.InventoryDomain) ReportFile {
	return &InventoryReportFile{data}
}

func (f *InventoryReportFile) Filename() string {
	date := time.Now().Format("2006-01-02")
	return "inventory_report_" + date + ".csv"
}

func (f *InventoryReportFile) Content() ([][]string, error) {
	var stock int
	var total float32

	headerLength := 8
	length := headerLength + len(f.data)
	result := make([][]string, length)

	date := time.Now().Format("02 January 2006")

	result[0] = []string{"LAPORAN NILAI BARANG"}
	result[1] = []string{}
	result[2] = []string{"Tanggal Cetak", date}
	result[3] = []string{"Jumlah SKU", fmt.Sprintf("%d", len(f.data))}
	result[6] = []string{}
	result[7] = []string{"SKU", "Nama Item", "Jumlah", "Rata-Rata Harga Beli", "Total"}

	for i, d := range f.data {
		stock += d.Stock
		total += d.Total()

		result[i+headerLength] = []string{
			d.SKU,
			d.Name,
			fmt.Sprintf("%d", d.Stock),
			fmt.Sprintf("%.0f", d.PurchasePriceAvg),
			fmt.Sprintf("%.0f", d.Total()),
		}
	}

	result[4] = []string{"Jumlah Total Barang", fmt.Sprintf("%d", stock)}
	result[5] = []string{"Total Nilai", fmt.Sprintf("%.0f", total)}

	return result, nil
}
