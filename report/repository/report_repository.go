package repository

import (
	"invenstore/report/domain"
	"time"
)

type ReportRepository interface {
	InventoryReport() ([]*domain.InventoryDomain, error)
	SalesReport(startAt, endAt time.Time) ([]*domain.SalesDomain, error)
}
