package repository

import (
	"database/sql"
	"invenstore/report/domain"
	"time"
)

type ReportRepositorySQLite struct {
	db *sql.DB
}

const (
	InventoryReportQuery = `
		SELECT p.product_id, pd.sku, pd.name, pd.stock, (SUM(p.total) / SUM(p.received_quantity)) as purchase_price_avg
		FROM purchases AS p
		JOIN products AS pd ON p.product_id = pd.id
		WHERE p.received_quantity > 0
		GROUP BY p.product_id
	`
	SalesReportQuery = `
		SELECT s.product_id, s.order_id, pd.sku, pd.name, s.quantity, s.price, s.purchase_price, s.total, s.created_at
		FROM sales AS s
		JOIN products AS pd ON s.product_id = pd.id
		WHERE s.created_at BETWEEN ? AND ?
		GROUP BY s.product_id
	`
)

func NewReportRepositorySQLite(db *sql.DB) ReportRepository {
	return &ReportRepositorySQLite{db}
}

func (r *ReportRepositorySQLite) InventoryReport() ([]*domain.InventoryDomain, error) {
	results := make([]*domain.InventoryDomain, 0)

	stmt, err := r.db.Prepare(InventoryReportQuery)
	if err != nil {
		return results, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return results, err
	}
	defer rows.Close()

	for rows.Next() {
		result := &domain.InventoryDomain{}
		err := rows.Scan(&result.ID, &result.SKU, &result.Name, &result.Stock, &result.PurchasePriceAvg)
		if err != nil {
			return results, err
		}

		results = append(results, result)
	}

	return results, nil
}

func (r *ReportRepositorySQLite) SalesReport(startAt, endAt time.Time) ([]*domain.SalesDomain, error) {
	results := make([]*domain.SalesDomain, 0)
	start := time.Date(startAt.Year(), startAt.Month(), startAt.Day(), 0, 0, 0, 0, startAt.Location())
	end := time.Date(endAt.Year(), endAt.Month(), endAt.Day(), 23, 59, 59, 0, endAt.Location())

	stmt, err := r.db.Prepare(SalesReportQuery)
	if err != nil {
		return results, err
	}
	defer stmt.Close()

	rows, err := stmt.Query(start.Unix(), end.Unix())
	if err != nil {
		return results, err
	}
	defer rows.Close()

	for rows.Next() {
		var createdAt int64

		result := &domain.SalesDomain{}
		err := rows.Scan(&result.ID, &result.OrderID, &result.SKU, &result.Name, &result.Quantity,
			&result.Price, &result.PurchasePrice, &result.Total, &createdAt)
		if err != nil {
			return results, err
		}

		result.CreatedAt = time.Unix(createdAt, 0)

		results = append(results, result)
	}

	return results, nil
}
