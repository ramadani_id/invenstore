package migration

import "database/sql"

type SQLiteMigration struct {
	db *sql.DB
}

func NewSQLiteMigration(db *sql.DB) Migration {
	return &SQLiteMigration{db}
}

func (m *SQLiteMigration) Migrate() error {
	migrations := []string{
		m.createProductsTable(),
		m.createPurchasesTable(),
		m.createSalesTable(),
	}

	for _, v := range migrations {
		err := func(sql string) error {
			stmt, err := m.db.Prepare(sql)
			if err != nil {
				return err
			}
			defer stmt.Close()

			if _, err = stmt.Exec(); err != nil {
				return err
			}

			return nil
		}(v)

		if err != nil {
			return err
		}
	}

	return nil
}

func (m *SQLiteMigration) createProductsTable() string {
	return `
		CREATE TABLE IF NOT EXISTS products (
			id INTEGER PRIMARY KEY,
			sku TEXT NOT NULL UNIQUE,
			name TEXT NOT NULL,
			stock INTEGER NOT NULL,
			created_at INTEGER NOT NULL,
			updated_at INTEGER NOT NULL
		)
	`
}

func (m *SQLiteMigration) createPurchasesTable() string {
	return `
		CREATE TABLE IF NOT EXISTS purchases (
			id INTEGER PRIMARY KEY,
			product_id INTEGER NOT NULL,
			quantity INTEGER NOT NULL,
			received_quantity INTEGER NULL,
			price REAL NOT NULL,
			total REAL NOT NULL,
			receipt_no TEXT NULL,
			status INTEGER NOT NULL,
			histories TEXT NULL,
			created_at INTEGER NOT NULL,
			updated_at INTEGER NOT NULL
		)
	`
}

func (m *SQLiteMigration) createSalesTable() string {
	return `
		CREATE TABLE IF NOT EXISTS sales (
			id INTEGER PRIMARY KEY,
			product_id INTEGER NOT NULL,
			quantity INTEGER NOT NULL,
			price REAL NOT NULL,
			purchase_price REAL NOT NULL,
			total REAL NOT NULL,
			order_id TEXT NULL,
			status INTEGER NOT NULL,
			created_at INTEGER NOT NULL,
			updated_at INTEGER NOT NULL
		)
	`
}
